//
//  String+Find.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import UIKit

extension String {
    
    /**
        - Verifying wheather string contains given substring or not.Ignoring case senstive
        - Parameter find: input sub string
        - Returns: returns boolean value
     */
    func containsIgnoringCase(find: String) -> Bool{
        return (self.range(of: find, options: .caseInsensitive) != nil)
    }
    
    /**
     - Parameter width: input cgfloat
     - Parameter font: input cgfloat
     - Returns: returns cgfloat value
     */
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }

}
