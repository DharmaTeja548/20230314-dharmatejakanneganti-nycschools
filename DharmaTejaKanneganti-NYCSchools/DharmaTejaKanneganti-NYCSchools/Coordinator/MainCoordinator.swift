//
//  MainCoordinator.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    unowned var navigationController:UINavigationController

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func schoolDetails(schoolName : String?, satDataModel : SchoolSATDataModel?, satDetailModel : SATDataDisplayModel?) {
        let detailVC = DetailsViewController.instantiate()
        detailVC.schoolName = schoolName
        detailVC.schoolSATModel = satDataModel
        detailVC.satDataDisplayModel = satDetailModel
        navigationController.pushViewController(detailVC, animated: true)
    }
}
