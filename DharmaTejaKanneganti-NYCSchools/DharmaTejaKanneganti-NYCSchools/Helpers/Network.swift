//
//  Network.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import Foundation

// This struct handles the network reachability status and error.

struct Network {
    static var reachability: Reachability!
    enum Status: String {
        case unreachable, wifi, wwan
    }
    enum Error: Swift.Error {
        case failedToSetCallout
        case failedToSetDispatchQueue
        case failedToCreateWith(String)
        case failedToInitializeWith(sockaddr_in)
    }
}
